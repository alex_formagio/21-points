/**
 * View Models used by Spring MVC REST controllers.
 */
package org.jhipster.healthy.web.rest.vm;
